﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Palindroms.Logic
{
    public static class StringExtension
    {
        public static bool IsPalindrome(this string imputString)
        {
            if (!string.IsNullOrWhiteSpace(imputString))
            {
                var length = imputString.Length;

                for (int i = 0; i < length / 2; i++)
                {
                    if (imputString[i] != imputString[length - i - 1])
                    {
                        return false;
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    }
