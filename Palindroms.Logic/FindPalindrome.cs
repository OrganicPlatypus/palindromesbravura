﻿using Spackle.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Palindroms.Logic
{
    public static class FindPalindrome
    {
        private static List<string> Find(uint length)
        {
            List<string> resultantList = new List<string>();
            foreach (var number in NumberGenerator.GenerateStringResponse(length))
            {
                if (number.IsPalindrome())
                {
                    resultantList.Add(number);
                }
            }

            return resultantList;
        }
        public static void PalindromLister()
        {
            for (var i = 5u; i < 6u; i++)
            {
                Console.Out.WriteLine(new Action(() =>
                {
                    var result = FindPalindrome.Find(i);
                    if (result != null)
                    {
                        Console.WriteLine(
                                "Palindrome size: {0}", i);
                        var counter = 1;
                        foreach (var item in result)
                        {
                            Console.WriteLine(
                                "Result of {0} Palintrom is: {1}", counter, item);
                            counter++;
                        }
                    }
                    else
                    {
                        Console.Out.WriteLine(
                            "Size: {0} - no palindrome found", i);
                    }
                }).Time());
                Console.ReadLine();
            }
        }
    }
}
